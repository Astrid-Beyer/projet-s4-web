# Projet S4 - Web

Projet réalisé dans le cadre du module Web du S4 en L2 Informatique

## Sujet

Site qui permet aux utilisateurs de trouver la race de chien idéal pour eux : en remplissant un formulaire, l'utilisateur définit un profil "type" à partir duquel le site peut lui suggérer un chien qui correspondrait à son profil. 

Si l'utilisateur se décrit dans le formulaire comme une personne sportive, le site lui suggérera une race de chien adaptée aux activités sportives. L'utilisateur peut renseigner plusieurs champs comme le fait de vivre en appartement, d'avoir des enfants... ces informations seront prises en compte pour le résultat du questionnaire.

Une fois le formulaire terminé, l'utilisateur tombe sur une page d'information décrivant la race de chien sélectionnée ainsi qu'une liste d'élevages disponibles en France de la race suggérée.

Un utilisateur peut être un éleveur (à indiquer à l'inscription), ce qui lui donne le droit de renseigner son élevage sur le site. Un utilisateur "normal" ne pourra pas proposer d'élevage. L'administrateur peut ajouter de nouvelles races de chien si nécessaire (ou en supprimer en cas d'erreur...).

### Version live du site

> https://trouve-ton-doggo.herokuapp.com/

## Contraintes

* Serveur web node.js / express
* Base de données SQLite 
    - Au moins trois tables
    - Possibilité de saisir des données depuis le site
* Gestion des utilisateurs
    - Possibilité de créer, supprimer, se connecter, se déconnecter
    - Au moins deux rôles d'utilisateurs connectés
    - Conforme à la RGPD
* Séparation Modèle-Vue-Controlleur
    - Vues avec des templates
    - Bootstrap
    - Formulaires
* Pas ou peu de JS côté client

## Pour tester

`npm i`

`node server.js`

Se rendre sur localhost:8000 dans son navigateur.

Si il y a des erreurs c'est probable que votre version de node n'est pas à jour. Pour régler ça:

`sudo npm i -g n`

`sudo n latest`

Puis relancer votre terminal.

## Lancer la base de données via un terminal

`sqlite3 database.sqlite`

---

## Dans users.json

Utilisateur Admin a pour mot de passe "admin" (mail : admin@admin.fr),

utilisateur Eleveur a pour mot de passe "toto" (mail : breeder@breeder.fr)

---

Astrid BEYER
