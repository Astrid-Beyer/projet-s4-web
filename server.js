const express = require('express');
const session = require('cookie-session');
//const helmet = require('helmet'); // protection contre attaques sur les en têtes HTTP
const mustache = require('mustache-express');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 8000;

const routes = require('./controllers/routes');

//Construction de l'app
const app = express();

//middlewares
function middlewareLogged(req, res, next) {
  if (req.session?.user) {
    res.locals.authenticated = true;
    switch(req.session.user.type){
      case 'admin':
        res.locals.isAdmin = true;
      case 'breeder':
        res.locals.isBreeder = true;
      break;
    }
  }
  else {
    res.locals.authenticated = false;
    res.locals.isAdmin = false;
    res.locals.isBreeder = false;
  }
  next();
};

app.use(express.json()); // Pour reconnaitre le body en tant qu'objet JSON
app.use(express.urlencoded({ extended: true }));
//app.use(helmet());
app.use(
    session({
      name: "userSession",
      keys: ["toto"],
      cookie: {
        secure: true,
        httpOnly: true
      },
    })
  );
app.use(middlewareLogged);
app.use(express.static('public'));

//settings
app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

//routes
app.use(routes);
app.use((req, res) => {
  res.status(404).render('error', {err: 'La page que vous cherchez est introuvable.'});
});

//Démarrage
app.listen(port, host, err => {
  if (err) console.error(err);
  else console.log(`Serveur en route sur ${host}:${port}`);
});