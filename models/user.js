/**
 *  user.js
 *  Définition de la table User
 *  @version 0.3
 */

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        pseudo:     DataTypes.STRING, // pseudo
        pw:         DataTypes.STRING, // mot de passe (password)
        mail:       DataTypes.STRING, // mail
        type :      DataTypes.STRING  //admin/ user/breeder
    });
};