/**
 *  breeding.js
 *  Définition de la table breeding (elevage)
 *  @version 0.4
 */

 module.exports = (sequelize, DataTypes) => {
    return sequelize.define('Breeding', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        place: DataTypes.STRING, // adresse
        name: DataTypes.STRING, // nom elevage
    });
};