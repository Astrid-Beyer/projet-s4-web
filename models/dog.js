/**
 *  dog.js
 *  Définition de la table Dog
 *  @version 0.3
 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Dog', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    breed: DataTypes.STRING,     // race
    playful: DataTypes.INTEGER,  // joueur
    obedient: DataTypes.INTEGER, // docile
    clean: DataTypes.INTEGER,    // propre
    calm: DataTypes.INTEGER,     // calme
    smart: DataTypes.INTEGER,    // intelligent
    kind: DataTypes.INTEGER,     // gentil (enfants, autres animaux)
    durable: DataTypes.INTEGER,  // robuste (santé)
    athletic: DataTypes.INTEGER, // besoin d'exercice
    lonely: DataTypes.INTEGER,   // supporte la solitude
    budget: DataTypes.INTEGER,   // budget
    fur: DataTypes.INTEGER,      // perd les poils
    apartment: DataTypes.INTEGER, // supporte les appartements
    description: DataTypes.TEXT,  // description
    image: DataTypes.STRING       // photo
  });
};