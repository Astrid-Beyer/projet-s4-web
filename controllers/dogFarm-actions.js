const { Breeding } = require("./sequelize");
const { getByID } = require("./user-actions.js");
const { getOne } = require("./dog-actions.js");

module.exports = {
	getAll: async ({idBreeder = null, userType = null}) => {
		return new Promise((resolve, reject) => {
			Breeding.findAll({ raw: true }).then(async (breedings) => {

				for(const breeding of breedings){
					if( (idBreeder && breeding.UserId == idBreeder) || userType == 'admin')
						breeding.isMine = true;
					const user = await getByID({id: breeding.UserId})
					breeding.user = user;
					
					const dog = await getOne({id: breeding.DogId})
					breeding.dog = dog.breed;
				}
				resolve(breedings);
			}).catch(err => {
				reject(err);
			});
		});
	},

	getByDogType: async ({DogId}) => {
		return new Promise((resolve, reject) => {
			Breeding.findAll({
				where: {
					DogId: DogId
				},
				raw: true
			}).then(async (breedings) => {

				for(const breeding of breedings){
					const user = await getByID({id: breeding.UserId})
					breeding.user = user;
				}
				resolve(breedings);
			}).catch(err => {
				reject(err);
			});
		});
	},

	create: async ({
		place,
		name,
		UserId,
		DogId
	}) => {
		const newBreeding = {
			place: place,
			name: name,
			UserId: UserId,
			DogId: DogId
		};
		return new Promise((resolve, reject) => {
			Breeding.create(newBreeding).then(breeding => {
				resolve(breeding.get({ plain: true }));
			}).catch(err => {
				reject(err);
			});
		});
	},

	delete: async ({ id }) => {
		return new Promise((resolve, reject) => {
			Breeding.destroy({
				where: {
					id: id
				}
			}).then(() => {
				resolve();
			}).catch(err => {
				reject(err);
			})
		});
	},

	update: async ({
		id,
		place,
		name,
		DogId
	}) => {
		const newBreeding = {
			place: place,
			name: name,
			DogId: DogId
		};

		return new Promise((resolve, reject) => {
			Breeding.update({
				place: newBreeding.place,
				name: newBreeding.name,
				DogId: newBreeding.DogId
			}, {
				where: {
					id: id
				}
			}).then(() => {
				resolve();
			}).catch(err => {
				reject(err);
			})
		})
	},
};
