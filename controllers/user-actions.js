const bcrypt = require('bcrypt');
const { User } = require("./sequelize");
const salt = '$2b$10$JzOZusrm5130vD8IalPCsu'; //bcrypt.genSaltSync(10);

module.exports = {

    register: async ({ pseudo, pw, mail, isBreeding = null }) => {
        const hash = bcrypt.hashSync(pw, salt);
        const newUser = {
            pseudo: pseudo,
            pw: hash,
            mail: mail,
            type: isBreeding ? "breeder" : "user"
        };
        return new Promise((resolve, reject) => {
            User.create(newUser).then(user => {
                const { pw, ...createdUser } = user.get({ plain: true });
                resolve(createdUser);
            }).catch(err => {
                reject(err);
            });
        });
    },

    get: async ({ mail, pw }) => {
        const hash = bcrypt.hashSync(pw, salt);
        return new Promise((resolve, reject) => {
            User.findOne({
                attributes: {
                    exclude: ['pw']
                },
                where: {
                    mail: mail,
                    pw: hash
                }
            }).then(user => {
                resolve(user?.get({ plain: true }));
            }).catch(err => {
                reject(err);
            });
        });
    },

    getByID: async ({ id }) => {
        return new Promise((resolve, reject) => {
            User.findByPk(id, {
                attributes: {
                    exclude: ['pw']
                }
            }).then(user => {
                resolve(user?.get({ plain: true }));
            }).catch(err => {
                reject(err);
            });
        });
    },

    delete: async ({ id }) => {
        return new Promise((resolve, reject) => {
            User.destroy({
                where: {
                    id: id
                }
            }).then(() => {
                resolve();
            }).catch(err => {
                reject(err);
            })
        });
    },
    changePassword: async ({ id, newPw }) => {
        const hash = bcrypt.hashSync(newPw, salt);
        return new Promise((resolve, reject) => {
            User.update({
                pw: hash
            }, {
                where: {
                    id: id
                }
            }).then(() => {
                resolve();
            }).catch(err => {
                reject(err);
            })
        })
    },
};
