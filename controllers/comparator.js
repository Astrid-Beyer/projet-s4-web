class Comparator{
    constructor(perfectDog, weighting){
        this.perfectDog = perfectDog;
        this.weighting = weighting;
        this. maxScore = 0;
        for(const [key, weight] of Object.entries(weighting)) {
            if(weight) this.maxScore += 4 * 2;
            else this.maxScore += 4;
        };
    }

    compareDog(testedDog){
        let dogScore = 0;

        dogScore += (4 - Math.abs(this.perfectDog.playful - testedDog.playful))     * (this.weighting.isPlayful ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.obedient - testedDog.obedient))   * (this.weighting.isObedient ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.clean - testedDog.clean))         * (this.weighting.isClean ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.calm - testedDog.calm))           * (this.weighting.isCalm ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.smart - testedDog.smart))         * (this.weighting.isSmart ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.kind - testedDog.kind))           * (this.weighting.isKind ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.durable - testedDog.durable))     * (this.weighting.isDurable ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.athletic - testedDog.athletic))   * (this.weighting.isAthletic ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.lonely - testedDog.lonely))       * (this.weighting.isLonely ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.budget - testedDog.budget))       * (this.weighting.isBudget ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.fur - testedDog.fur))             * (this.weighting.isFur ? 2 : 1);
        dogScore += (4 - Math.abs(this.perfectDog.apartment - testedDog.apartment)) * (this.weighting.isApartment ? 2 : 1);
        
        testedDog.percentage = ( (dogScore / this.maxScore) * 100).toFixed(2);
        return dogScore;
    };
};

module.exports = Comparator;