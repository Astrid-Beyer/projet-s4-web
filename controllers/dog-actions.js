const Comparator = require("./comparator");
const { Dog } = require("./sequelize");

module.exports = {

  getAll: async () => {
    return new Promise((resolve, reject) => {
      Dog.findAll({ raw: true }).then((dogs) => {
        resolve(dogs);
      }).catch(err => {
        reject(err);
      });
    });
  },

  getBest: async ({
    playful,
    obedient,
    clean,
    calm,
    smart,
    kind,
    durable,
    athletic,
    lonely,
    budget,
    fur,
    apartment,
    isPlayful = null,
    isObedient = null,
    isClean = null,
    isCalm = null,
    isSmart = null,
    isKind = null,
    isDurable = null,
    isAthletic = null,
    isLonely = null,
    isBudget = null,
    isFur = null,
    isApartment = null
  }) => {
    const perfectDog = Dog.build({
      breed: "good boy",
      playful: playful,
      obedient: obedient,
      clean: clean,
      calm: calm,
      smart: smart,
      kind: kind,
      durable: durable,
      athletic: athletic,
      lonely: lonely,
      budget: budget,
      fur: fur,
      apartment: apartment,
      description: "The perfect dog",
      image: ''
    });

    const comparatorWeighting = {
      isPlayful: isPlayful,
      isObedient: isObedient,
      isClean: isClean,
      isCalm: isCalm,
      isSmart: isSmart,
      isKind: isKind,
      isDurable: isDurable,
      isAthletic: isAthletic,
      isLonely: isLonely,
      isBudget: isBudget,
      isFur: isFur,
      isApartment: isApartment
    };

    const c = new Comparator(perfectDog, comparatorWeighting);
    return new Promise((resolve, reject) => {
      Dog.findAll({ raw: true }).then((dogs) => {
        dogs.sort((dog1, dog2) => {
          return c.compareDog(dog2) - c.compareDog(dog1);
        });
        resolve(dogs.slice(0, 3)); // Les 3 plus hauts
      }).catch(err => {
        reject(err);
      });
    });
  },

  create: async ({
    breed,
    playful,
    obedient,
    clean,
    calm,
    smart,
    kind,
    durable,
    athletic,
    lonely,
    budget,
    fur,
    apartment,
    description,
    image
  }) => {
    const newDog = {
      breed: breed,
      playful: playful,
      obedient: obedient,
      clean: clean,
      calm: calm,
      smart: smart,
      kind: kind,
      durable: durable,
      athletic: athletic,
      lonely: lonely,
      budget: budget,
      fur: fur,
      apartment: apartment,
      description: description,
      image: image
    };
    return new Promise((resolve, reject) => {
      Dog.create(newDog).then(dog => {
        resolve(dog.get({ plain: true }));
      }).catch(err => {
        reject(err);
      });
    });
  },

  delete: async ({ id }) => {
    return new Promise((resolve, reject) => {
      Dog.destroy({
        where: {
          id: id
        }
      }).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      })
    });
  },

  update: async ({
    id,
    playful,
    obedient,
    clean,
    calm,
    smart,
    kind,
    durable,
    athletic,
    lonely,
    budget,
    fur,
    apartment,
    description,
    image
  }) => {
    const newDog = {
      breed: breed,
      playful: playful,
      obedient: obedient,
      clean: clean,
      calm: calm,
      smart: smart,
      kind: kind,
      durable: durable,
      athletic: athletic,
      lonely: lonely,
      budget: budget,
      fur: fur,
      apartment: apartment,
      description: description,
      image: image
    };

    return new Promise((resolve, reject) => {
      Dog.update({
        breed: newDog.breed,
        playful: newDog.playful,
        obedient: newDog.obedient,
        clean: newDog.clean,
        calm: newDog.calm,
        smart: newDog.smart,
        kind: newDog.kind,
        durable: newDog.durable,
        athletic: newDog.athletic,
        lonely: newDog.lonely,
        budget: newDog.budget,
        fur: newDog.fur,
        apartment: newDog.apartment,
        description: newDog.description,
        image: newDog.image
      }, {
        where: {
          id: id
        }
      }).then(() => {
        resolve();
      }).catch(err => {
        reject(err);
      })
    })
  },

  getOne: async ({ id }) => {
    return new Promise((resolve, reject) => {
      Dog.findByPk(id).then(dog => {
        resolve(dog?.get({ plain: true }));
      }).catch(err => {
        reject(err);
      });
    });
  },

};
