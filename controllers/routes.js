const express = require("express");
const router = express.Router();
const User = require("./user-actions.js");
const Dog = require("./dog-actions.js");
const DogFarm = require("./dogFarm-actions.js");

const typeLevels = Object.freeze({
    'guest': 0,
    'user': 1,
    'breeder': 2,
    'admin': 3
});

/** Middleware d'autorisation
 * @param {string} type - Le type autorisé à accéder à la route
 * @param {bool} [canBeViewedFromAbove = true] - Si les types avec un plus haut niveau d'accès peuvent y accèder (par défaut à vrai)
 * */ 
const authorizedType = (type, canBeViewedFromAbove = true) => {
    return authorizedType[type] || (authorizedType[type] = function(req, res, next) {

        const operation = Object.freeze({
            true : function(a, b) { return a > b },
            false: function(a, b) { return a != b },
        });

        if (req.session?.user == null)
            if(typeLevels[type] == typeLevels['guest']) next();
            else res.status(401).render('error', {err: 'Accès non autorisé.'});
        else {
            if (operation[canBeViewedFromAbove](typeLevels[type],typeLevels[req.session.user.type]))
                res.status(401).render('error', {err: 'Accès non autorisé.'});
            else next();
        }
    });
};

router.get('/', authorizedType('guest'), function (req, res) {
    res.status(200).render('home', { home: true });
});

router.get('/test', authorizedType('guest'), function (req, res) {
    res.status(200).render('dogForm', { test: true });
});

router.get('/elevages', authorizedType('guest'), function (req, res) {
    let userId, userType;
    if(req.session?.user){
        if(typeLevels[req.session.user.type] >= typeLevels['breeder']){
            userId = req.session.user.id;
            userType = req.session.user.type;
        }
    }
    DogFarm.getAll({idBreeder: userId, userType: userType}).then( farms => {
        Dog.getAll().then((dogs) => {
            res.status(200).render('dogFarm', { elevages: true, farms: farms, dogs: dogs });
        }).catch(err => {
            res.status(501).render('error', {err: err});
        });
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.get('/profil', authorizedType('user'), (req, res) => {
    res.status(200).render('profile', { profil: true, user: req.session.user });
});

router.get('/connexion', authorizedType('guest', false), function (req, res) {
    res.status(200).render('login');
});

router.post('/login', authorizedType('guest', false), function (req, res) {
    User.get(req.body).then(user => {
        if(user){
            req.session.user = user;
            res.status(200).redirect('/');
        }
        else res.status(403).render('login', {error: 'Identifiant ou mot de passe incorrect.'});
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.get('/inscription', authorizedType('guest', false), function (req, res) {
    res.status(200).render('register');
});

router.post('/register', authorizedType('guest', false), function (req, res) {
    User.register(req.body).then(user => {
        req.session.user = user;
        res.status(201).redirect('/');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.get('/logout', authorizedType('user'), function(req, res) {
    req.session = null;
    res.status(200).redirect('/');
});

router.get('/changePassword', function(req, res) {
    res.status(200).render('changePassword');
});

router.post('/deleteUser', authorizedType('user'), function (req, res) {
    User.delete({
        id: req.session.user.id
    }).then(() => {
        req.session = null;
        res.status(200).redirect('/');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.post('/changePassword', authorizedType('user'), function (req, res) {
    User.get({ mail: req.session.user.mail, pw: req.body.oldpw }).then(user => {
        if (user) {
            User.changePassword({ id: req.session.user.id, newPw: req.body.newpw }).then(() => {
                res.status(200).render('profile');
            }).catch(err => {
                res.status(501).render('error', { err: err });
            })
        }
        else res.status(403).render('changePassword', { error: 'Mot de passe incorrect.' });
    }).catch(err => {
        res.status(501).render('error', { err: err });
    });
});

router.post('/searchBestDog', authorizedType('guest'), function (req, res) {
    Dog.getBest(req.body).then((dogs) => {
        const [dog, ...othersDogs] = dogs;
        DogFarm.getByDogType({DogId: dog.id}).then((farms) => {
            res.status(200).render('result', {dog: dog, othersDogs: othersDogs, farms: farms, moreThanOne: (farms.length > 1)});
        }).catch(err => {
            res.status(501).render('error', {err: err});
        });
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.get('/chien', authorizedType('guest'), function (req, res) {
    if(req.query.id == null) res.status(404).render('error', {err: "Le chien que vous chercher est inexistant"});
    else{
        Dog.getOne({id: req.query.id}).then((dog) => {
            if(dog) {
                DogFarm.getByDogType({DogId: dog.id}).then((farms) => {
                    res.status(200).render('dogViewer', {dog: dog, farms: farms, moreThanOne: (farms.length > 1)});
                }).catch(err => {
                    res.status(501).render('error', {err: err});
                });
            }
            else res.status(404).render('error', {err: "Le chien que vous chercher est inexistant"});
        }).catch(err => {
            res.status(501).render('error', {err: err});
        });
    }
});

router.post('/deleteFarm/:id', authorizedType('breeder'), function (req, res) {
    DogFarm.delete({
        id: req.params.id
    }).then(() => {
        res.status(200).redirect('/elevages');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.post('/addFarm', authorizedType('breeder'), function (req, res) {
    const newDogFarm = { ...req.body, UserId: req.session.user.id };
    DogFarm.create(newDogFarm).then(() => {
        res.status(201).redirect('/elevages');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.post('/updateFarm/:id', authorizedType('breeder'), function (req, res) {
    const farmUpdateRequest = { ...req.body, id: req.params.id };
    DogFarm.update(farmUpdateRequest).then(() => {
        res.status(201).redirect('/elevages');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.get('/admin', authorizedType('admin'), function (req, res) {
    Dog.getAll().then((dogs) => {
        res.status(200).render('admin', { admin: true, dogs: dogs });
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.post('/deleteDog/:id', authorizedType('admin'), function (req, res) {
    Dog.delete({
        id: req.params.id
    }).then(() => {
        res.status(200).redirect('/admin');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

router.post('/addDog', authorizedType('admin'), function (req, res) {
    Dog.create(req.body).then(() => {
        res.status(201).redirect('/admin');
    }).catch(err => {
        res.status(501).render('error', {err: err});
    });
});

module.exports = router;