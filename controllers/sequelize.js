const DogModel = require('../models/dog');
const UserModel = require('../models/user');
const BreedingModel = require('../models/breeding');
const { Sequelize } = require('sequelize');

const dogs = require('../models/dogs.json');
const users = require('../models/users.json');
const farms = require('../models/farms.json');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'database.sqlite'
});

const Dog       = DogModel(sequelize, Sequelize);
const User      = UserModel(sequelize, Sequelize);
const Breeding  = BreedingModel(sequelize, Sequelize);

Breeding.belongsTo(Dog, {onDelete: 'CASCADE'}); // un élevage a un chien
Breeding.belongsTo(User, {onDelete: 'CASCADE'}); // un élevage appartient à un éleveur

sequelize.sync().then(async () => { //{force: true} dans sync pour reset la bdd
  await Dog.findAll().then((dbDogs) => {
    if (dbDogs.length == 0) {
      Dog.bulkCreate(dogs);
    }
  });
  await User.findAll().then((dbUsers) => {
    if (dbUsers.length == 0) {
      User.bulkCreate(users);
    }
  });
  await Breeding.findAll().then((dbBreedings) => {
    if (dbBreedings.length == 0) {
      Breeding.bulkCreate(farms);
    }
  });
  console.log(`BDD et tables crées`);
});

module.exports = {
  Dog,
  User,
  Breeding
};
